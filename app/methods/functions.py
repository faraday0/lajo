import requests, _thread
from app import config, request, models, db, mail, app
from urllib.parse import quote
from unique_id import get_unique_id
from flask_mail import Message

cfg = config['production']

def send_sms(phone,message):
    request_url = "http://api.ebulksms.com:8080/sendsms?username=%s&apikey=%s&sender=%s&messagetext=%s&flash=0&recipients=%s" % \
    (cfg.SMS_USERNAME, cfg.SMS_PASSWORD, cfg.SMS_SENDER, quote(message), phone )

    # request_url = "http://smsclone.com/components/com_spc/smsapi.php?username=%s&password=%s&sender=%s&recipient=%s&message=%s" % \
    # (cfg.SMS_USERNAME, cfg.SMS_PASSWORD, cfg.SMS_SENDER, phone, quote(message) )

    _thread.start_new_thread(do_send_sms, (request_url,))

def do_send_sms(url):
    r = requests.get(url)
    print(r.text)


def send_mail(subject, message, html, recipient, sender):
    msg = Message(subject, recipients=[recipient], body=message, sender="%s <info@invoo.com>" % sender, html=html)
    new_thread = Thread(target=send_now, args=[app, msg])
    new_thread.start()

    if not new_thread:
        print("Failed to send mail")

    else:
        print("New thread started")


def send_now(app_c, msg):
    with app_c.app_context():
        print("New thread Run")
        mail.send(msg)

def log_internal(event, user_id=None, amount=None):
    log = models.LogsInternal(event=event,user_id=user_id,amount=amount)
    db.session.add(log)
    db.session.commit()

def log_external(event,type,user,other,amount):
    log = models.LogsExternal(event_type=type,event=event,user_id=user,other_id=other,amount=amount)
    db.session.add(log)
    db.session.commit()

def get_user_details(user_id):
    user                            = models.User.query.filter_by(id=user_id).first()
    return_data                     = user.as_dict()
    return_data['balance']          = user.wallet.first().balance
    return_data['last_updated']     = user.wallet.first().updated
    cycle                           = user.cycle.filter_by(active=True).first()
    return_data['cycle']            =  cycle.as_dict() if cycle else None
    return return_data

def generate_voucher_code(length=12):
    return  get_unique_id(length=length,excluded_chars='0Oo\!|><+)(*$\'"?`~}p%/;:.=,[@]{-&^_#')