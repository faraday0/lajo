from app import db,create_access_token, jsonify
from app.methods import functions as Repo
from werkzeug.security import generate_password_hash, check_password_hash
import uuid, random, datetime



class BankDetails(db.Model):
    __tablename__   = "bank_details"
    id              = db.Column(db.Integer, primary_key=True)
    owner_id        = db.Column(db.Integer, db.ForeignKey('users.id'))
    account_number  = db.Column(db.String)
    account_name    = db.Column(db.String)
    bank            = db.Column(db.String)
    bank_id         = db.Column(db.String)

    def as_dict(self):
        return {c.name: str(getattr(self, c.name)) for c in self.__table__.columns}


class User(db.Model):
    __tablename__ 	        = 'users'
    bank_account	        = db.relationship('BankDetails', backref='owner', lazy='dynamic')
    confirmation 	        = db.Column(db.Integer, default=0)
    email 			        = db.Column(db.String)
    id 				        = db.Column(db.Integer, primary_key = True)
    is_agent		        = db.Column(db.Boolean, default=False)
    name 			        = db.Column(db.String)
    password_hash 	        = db.Column(db.String)
    phone 			        = db.Column(db.String, unique=True)
    public_id 		        = db.Column(db.String, unique=True)
    status 			        = db.Column(db.Boolean, default=False)
    agent_status 	        = db.Column(db.Boolean, default=False)
    wallet                  = db.relationship('Wallet', backref='owner', lazy='dynamic')
    cycle          	        = db.relationship('Cycle', backref='owner', lazy='dynamic')
    agent          	        = db.relationship('AgentRequest', backref='agent', lazy='dynamic')
    contact                 = db.relationship('Contact', backref='agent', lazy='dynamic')
    card                    = db.relationship('Voucher', backref='user', lazy='dynamic')

    def set_uuid(self):
        self.public_id = str(uuid.uuid4())

    @property
    def password(self):
        return 'password is not readable'

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)
        self.set_uuid()

    @property
    def is_active(self):
        return self.status

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def generate_jwt(self):
        return create_access_token(self.public_id)

    def generate_code(self):
        code = random.randint(100005,999999)
        self.confirmation = code
        return code

    def verify_code(self, code):
        return self.confirmation == int(code)

    def create_wallet(self):
        new_wallet = Wallet(owner_id = self.id)
        db.session.add(new_wallet)
        db.session.commit()
        Repo.log_internal('credit_wallet - %d' % self.id)

    def create_cycle(self, ending, amount, time_period = None):
        new_cycle = Cycle(user_id = self.id, ending = ending, time_period = time_period, amount=amount)
        db.session.add(new_cycle)
        db.session.commit()
        Repo.log_internal('Created Cycle for %d on %r' % (self.id, datetime.datetime.utcnow()))

    def as_dict(self):
        userData = dict()
        userData['name'] 			= self.name
        userData['id'] 				= self.public_id
        userData['is_agent'] 		= self.is_agent
        userData['phone']			= self.phone
        userData['status']			= self.status
        userData['agent_status'] 	= self.agent_status or False
        return userData


class Wallet(db.Model):
    __tablename__   = 'wallets'
    balance         = db.Column(db.REAL, default=0)
    id              = db.Column(db.Integer, primary_key=True)
    owner_id        = db.Column(db.Integer,db.ForeignKey('users.id'))
    prev_balance    = db.Column(db.REAL, default=0)
    updated         = db.Column(db.DateTime, default=datetime.datetime.utcnow())

    def repr(self):
        return self.owner_id

    def increment(self, value):
        self.prev_balance 	= self.balance
        self.balance 		= self.balance + float(value)
        self.updated		= datetime.datetime.utcnow()
        return True

    def decrease(self, value):
        self.prev_balance 	= self.balance

        '''
         1. Check that the guy is not a merchant
         2. if merchant, just check that their amount to deduct is <= the balance
         3. Else use the percentage to calculate how much they can withdraw 
         4. return of false means insufficient balance 
        '''
        check = self.balance - float(value)
        if check > 0:
            self.balance 		= self.balance - float(value)
            self.updated		= datetime.datetime.utcnow()
            return True
        else:
            return False

    def can_withdraw(self, value):
        if(self.balance > float(value)):
            return True
        else:
            return False

    def as_dict(self):
        return {c.name: str(getattr(self, c.name)) for c in self.__table__.columns}



class LogsInternal(db.Model):
    __tablename__ 	= 'logs_internal'
    id              = db.Column(db.Integer, primary_key=True)
    user_id      	= db.Column(db.String)
    event 			= db.Column(db.String)
    amount 			= db.Column(db.REAL)
    time 			= db.Column(db.DateTime, default=datetime.datetime.utcnow())

    def as_dict(self):
        return {c.name: str(getattr(self, c.name)) for c in self.__table__.columns}


class LogsExternal(db.Model):
    __tablename__ 	= 'logs_external'
    id              = db.Column(db.Integer, primary_key=True)
    user_id      	= db.Column(db.Integer,db.ForeignKey('users.id'))
    other_id 		= db.Column(db.Integer)
    event 			= db.Column(db.String)
    event_type		= db.Column(db.String)
    amount 			= db.Column(db.REAL)
    time 			= db.Column(db.DateTime, default=datetime.datetime.utcnow())
    
    def as_dict(self):
        return {c.name: str(getattr(self, c.name)) for c in self.__table__.columns}


class Cycle(db.Model):
    __tablename__ 	= 'cycle'
    id 				= db.Column(db.Integer, primary_key=True)
    user_id 		= db.Column(db.Integer, db.ForeignKey('users.id'))
    time_period 	= db.Column(db.String, default='1 Month')
    started 		= db.Column(db.DateTime, default=datetime.datetime.utcnow())
    ending 			= db.Column(db.DateTime)
    created 		= db.Column(db.DateTime, default=datetime.datetime.utcnow())
    amount 			= db.Column(db.REAL)
    active 			= db.Column(db.Boolean, default=True)

    def as_dict(self):
        return {c.name: str(getattr(self, c.name)) for c in self.__table__.columns}


class AgentRequest(db.Model):
    __tablename__ 	= 'agent_requests'
    id 				= db.Column(db.Integer, primary_key=True)
    user_id 		= db.Column(db.Integer, db.ForeignKey('users.id'))
    created 		= db.Column(db.DateTime, default=datetime.datetime.utcnow())
    handled 		= db.Column(db.Boolean, default=False)


class AdminAccounts(db.Model):
    __tablename__ 	= 'admins'
    id 				= db.Column(db.Integer, primary_key=True)
    created 		= db.Column(db.DateTime, default=datetime.datetime.utcnow())
    email 			= db.Column(db.String)
    password_hash 	= db.Column(db.String)

    @property
    def password(self):
        return 'password is not readable'

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)


class Contact(db.Model):
    __tablename__   = "contacts"
    id 				= db.Column(db.Integer, primary_key=True)
    created 		= db.Column(db.DateTime, default=datetime.datetime.utcnow())
    contact_id      = db.Column(db.Integer, db.ForeignKey('users.id'))
    owner_id        = db.Column(db.Integer)


class Voucher(db.Model):
    __tablename__   = "vouchers"
    id 				= db.Column(db.Integer, primary_key=True)
    batch           = db.Column(db.String)
    amount          = db.Column(db.REAL)
    code            = db.Column(db.String, unique=True)
    used            = db.Column(db.Boolean, default=False)
    used_by         = db.Column(db.Integer, db.ForeignKey('users.id'))
    created         = db.Column(db.DateTime, default=datetime.datetime.utcnow())