from flask import Flask, render_template, Blueprint, request, jsonify, g, session, redirect
from flask_jwt import JWT, jwt_required, current_identity	
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS 
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager, Server
from config import config
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_identity, decode_token
)
from flask_mail import Mail


app 		= Flask(__name__)
app.config.from_object(config['production'])

db 			= SQLAlchemy(app)
migrate 	= Migrate(db)
jwt 		= JWTManager(app)
manager 	= Manager(app)
migrate 	= Migrate(app,db)
mail        = Mail(app)

manager.add_command('db', MigrateCommand)
manager.add_command('run', Server(host='0.0.0.0'))

CORS(app)


from app import models

# Register bleprints
from app.apis.views import api 
from app.apis.auth.views import auth
from app.admin.views import admin

app.register_blueprint(api, url_prefix='/api/v1/')
app.register_blueprint(auth, url_prefix='/api/v1/auth/')
app.register_blueprint(admin, url_prefix='/admin')

#Error handlers

@app.errorhandler(404)
def page_not_found(e):
    return jsonify({'status':'error', 'message':'404 not found'}), 404

@app.errorhandler(400)
def bad_request(e):
    return jsonify({'status':'error', 'message':'400 Bad Request'}), 400


@app.errorhandler(500)
def internal_server_error(e):
    return jsonify({'status':'error', 'message':'Internal Server Error'}), 500

@app.errorhandler(405)
def method_not_allowed(e):
    return jsonify({'status':'error', 'message':'Not allowed'}), 405


@app.route('/<path:dummy>')
def wildcard(dummy):
    return render_template('default.html')

@app.route('/')
def wild():
    return render_template('default.html')