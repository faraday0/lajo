from app import app, Blueprint, request, models, db, jsonify, session, jwt_required, config, decode_token, g, render_template, redirect
from app.methods import functions as Repo
import random, string, requests, datetime

admin = Blueprint('admin', __name__)

@admin.route('/login',methods=['POST', 'GET'])
def admin_login():
    if request.method == 'POST':
        email       = request.form.get('email')
        password    = request.form.get('password')
        check_user = models.AdminAccounts.query.filter_by(email=email).first()
        if check_user and check_user.verify_password(password):
            session['email'] = email
            return redirect('/admin/')
        else:
            return "Username or Password is incorrect"
    else:
        return render_template('login.html')

@admin.route('/')
def dashboard():
    if 'email' not in session:
        return redirect('/admin/login')

    waiting_list = models.AgentRequest.query.filter_by(handled=False).all()
    return render_template('admin.html', waiting_list=waiting_list)


@admin.route('/approve/<ad_request>')
def approve(ad_request):
    if 'email' not in session:
        return redirect('/admin/login')
    req = models.AgentRequest.query.get(ad_request)

    user_object = req.agent
    if user_object:
        user_object.agent_status = True
        req.handled = True
        db.session.commit()
        return redirect('/admin/')
    else:
        pass


@admin.route('/logout')
def logout():
    session.pop('email')
    session.clear()
    return redirect('/login')


@admin.route('/voucher/generate', methods=['POST','GET'])
def generate_voucher():
    if 'email' not in session:
        return redirect('/admin/login')

    #voucher quantity and amount will be determined from form 
    amount      = request.form.get('amount')
    batch       = datetime.datetime.now()
    quantity    = int(request.form.get('quantity'))

    #initialize Empty Insert Object to 
    insert_obj  = []

    for i in range(quantity):
        voucher = Repo.generate_voucher_code(8)
        insert_obj.append(models.Voucher(code=voucher,amount=amount,batch=batch))

    db.session.bulk_save_objects(insert_obj)
    db.session.commit()
    return redirect('/admin')