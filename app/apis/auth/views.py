from app import app, Blueprint, request, models, db, jsonify, jwt_required, config
from app.methods import functions as Repo

auth = Blueprint('auth', __name__)

@auth.route('register',  methods=['POST'])
def register():
	name 		= request.json['name']
	phone 		= request.json['phone']	
	password 	= request.json['password']
	email 		= ''
	if request.json.get('email'):
		email 	= request.json['email']

	checkUser 	= models.User.query.filter_by(phone=phone).first()

	if not checkUser:
		newUser = models.User(name=name,phone=phone,email=email,password=password,is_agent=True)
		db.session.add(newUser)
		db.session.commit()
		newUser.create_wallet()
		
		code = newUser.generate_code()
		Repo.send_sms(phone,'Congratulations, your registration on %s was successful, your code is %d' % (config['production'].APP_NAME, code))

		newAgentRequest = models.AgentRequest(user_id=newUser.id)
		db.session.add(newAgentRequest)
		db.session.commit()
		return jsonify({"status" : "success", "token" : newUser.generate_jwt(), "data" : newUser.as_dict()}), 200
	else:
		return jsonify({"status" : "error", "message" : "Phone number has already been registered"}), 403


@auth.route('login', methods=['POST'])
def login():
	phone 		= request.json['phone']	
	password 	= request.json['password']

	checkUser 	= models.User.query.filter_by(phone=phone).first()

	if checkUser and checkUser.verify_password(password):
		if checkUser.is_active:
			user_details = Repo.get_user_details(checkUser.id)
			return jsonify({"status" : "success", "message" : "login successful", "token" : checkUser.generate_jwt(), "user" : user_details})
		else:
			return jsonify({"status" : "error", "message" : "Your account is not active yet"}), 425
	else:
		return jsonify({"status" : "error", "message" : "Username or password incorrect"}), 400


@auth.route('code/verify', methods=['POST'])
@auth.route('verify', methods=['POST'])
def verify_token():
	code = request.json['code']
	user = models.User.query.filter_by(public_id=request.json['user_id']).first()
	if user and str(code) == str(user.confirmation):
		user.status = True 
		user.confirmation = None
		db.session.commit()
		return jsonify({"status" : "success", "message" : "Your account has been verified", "data" : user.as_dict()}), 200

	else:
		return jsonify({"status" : "error", "message" : "Please check Code and retry"}), 400


@auth.route('code/generate', methods=['POST'])
def generate_auth_code():
	phone 	= request.json['phone']
	user 	= models.User.query.filter_by(phone=phone).first()
	if user:
		code 	= user.generate_code()
		Repo.send_sms(phone,"Your Code is %d" % code)
		return jsonify({"status":"success", "message":"Code generated", "data":user.as_dict()}), 200
	else:
		return jsonify({"status":"error", "message":"Phone number is not registered"}), 400


@auth.route('verify/<public_id>', methods=['POST'])
def verify(public_id):
	code = request.json['access_code']
	user = models.User.query.filter_by(public_id=public_id).first()

	if user and str(code) == str(user.confirmation):
		user.status = True 
		user.confirmation = None
		db.session.commit()
		return jsonify({"status" : "success", "message" : "Your account has been verified"}), 200

	else:
		return jsonify({"status" : "error", "message" : "Please check Code and retry"}), 400


