from app import app, Blueprint, request, models, db, jsonify, jwt_required, config, decode_token, g
from app.methods import functions as Repo
import random, string, requests, datetime

api = Blueprint('api', __name__)


def exclude_from_token_check(func):
	func._exclude_from_token_check = True
	return func

# def exclude_from_token_check(func):
#     def func_wrapper(func):
#         func._exclude_from_token_check = True
#     return func_wrapper

@api.before_request
def token_check(*args, **kwargs):
	# this method was running even when the request was 'options' disrupting normal app flow
	check_token = False
	if request.endpoint in app.view_functions:
	    view_func = app.view_functions[request.endpoint]
	    check_token = not hasattr(view_func, '_exclude_from_token_check')
	    print(check_token)

	if request.method != 'OPTIONS' and check_token:
		token 			= request.headers.get('Authorization').replace('Bearer ','')
		public_id 		= decode_token(token)['identity']
		if public_id:
			user = models.User.query.filter_by(public_id=public_id).first()
			if user:
				g.user = user
		else:
			return jsonify({"status" : "error", "message" : "unauthorized, please login again"}), 403


@api.route('transaction/history')
def history():
	return_data = []
	logs = models.LogsExternal.query.filter_by(user_id=g.user.id).all()

	for log in logs:
		data = log.as_dict()
		user =  models.User.query.filter_by(id=log.other_id).first()
		data['user'] = 'No User'
		if user is not None:
			data['user'] = user.name
		return_data.append(data)

	return jsonify({"status" : "success", "data": return_data})

@api.route('user/create', methods=['POST'])
def create_user():
	name 		= request.json.get('name')
	phone 		= request.json.get('phone')
	tenor 		= int(request.json.get('date'))
	amount 		= int(request.json.get('target'))
	end_date 	= datetime.datetime.now() + datetime.timedelta(tenor)
	checkUser 	= models.User.query.filter_by(phone=phone).first()

	if(checkUser):
		return jsonify({"status":"error", "message":"User you are trying to regiser already exists"}), 400

	if(len(name)!=0 and len(phone) > 9):
		password 	= ''.join(random.choice(string.ascii_uppercase) for x in range(6))
		newUser 	= models.User(name=name, phone=phone, password=password)
		db.session.add(newUser)
		db.session.commit()
		newUser.create_wallet()
		code = newUser.generate_code()
		newUser.create_cycle(ending = end_date, amount = amount, time_period = '%d Days' % int(tenor))
		Repo.send_sms(phone, "Your password is %s" % (password))
		return jsonify({"status":"success", "message":"User added", "data":newUser.as_dict()})
	else:
		return jsonify({"status":"error", "message":"Incorrect input"}), 400



@api.route('user/bank', methods=['POST'])
def set_bank_details():
	user 			= request.json.get('user_id')
	bank_id 		= request.json.get('bank_code')
	bank 			= request.json.get('bank_name')
	account_number 	= request.json.get('account_number')
	account_name 	= request.json.get('account_name')

	if(len(account_number)<10 or len(bank_id)<1):
		return jsonify({"status":"error", "message":"please check your input"}), 400
	user_id 		= models.User.query.filter_by(public_id=user).first().id
	newBankDetails 	= models.BankDetails(owner_id=user_id,account_number=account_number,
						bank_id=bank_id,bank=bank,account_name=account_name)

	db.session.add(newBankDetails)
	db.session.commit()

	return jsonify({'status':"success", "message":"Bank details have been added"})


@api.route('banks', methods=['GET'])
def get_bank_details():
	req = requests.post('http://staging1flutterwave.co:8080/pwc/rest/fw/banks/')
	return jsonify(req.text)
	

@api.route('withdraw/balance', methods=["GET","POST"])
def get_user_balance():
	phone 	= request.json.get('phone')
	amount 	= request.json.get('amount')
	user 	= models.User.query.filter_by(phone=phone).first()

	if user is not None and user.is_agent is not True:
		Repo.log_internal("tried to withdraw for %s" % str(phone), g.user.id, amount)
		if(user.wallet.first().can_withdraw(amount)):
			code = user.generate_code()
			Repo.send_sms(user.phone, "There's a pending debit of N%s on your account, use code %s to authorise" % (str(amount), str(code)))
			return jsonify({"status" : "success", "data": user.wallet.first().as_dict(), "message":"user found"})
		else:
			return jsonify({"error":"success", "message":"cannot withdraw yet"}), 400
	else:
		return jsonify({"status":"error", "message":"user not found"}), 404


@api.route('withdraw/confirm', methods=["POST"])
def confirm_withdrawal():
	phone 	= request.json.get('phone')
	amount 	= request.json.get('amount')
	user 	= models.User.query.filter_by(phone=phone).first()
	code 	= request.json.get('code')
	if str(code) == str(user.confirmation):
		if user.wallet.first().decrease(float(amount)):
			user.confirmation=None
			Repo.log_internal("debit on user id: %s " % str(user.id), g.user.id, amount)
			Repo.log_external("You withdrew "+amount, "debit", user.id, g.user.id, amount)
			Repo.log_external("You helped %s withdraw  %s" % (user.name, str(amount)), "none", g.user.id, user.id, amount)
			Repo.send_sms(user.phone, "a successful debit of %s occurred on your account" % str(amount))
			return jsonify({"status": "success", "message": "debit successful"})
		else:
			return jsonify({"status":"error", "message": "cannot debit"}), 403
	else:
		Repo.log_internal("Incorrect code on user id:  %s " % str(user.id), g.user.id, amount)		
		return jsonify({"status":"error", "message": "confirmation code is not correct"}), 400


@api.route('log', methods=["GET","POST"])
def log_action():
	action = request.json.get('event')
	amount = request.json.get('amount')
	log_event = models.LogsInternal(user_id=g.user.id,event=action,amount=amount)
	db.session.add(log_event)
	db.session.commit()
	return jsonify("ok")


@api.route('wallet/fund', methods=["POST"])
def fund_wallet():
	amount 	= float(request.json.get('amount'))
	phone 	= request.json.get('phone')
	user 	= models.User.query.filter_by(phone=phone).first()
	if user is not None:
		if g.user.wallet.first().balance > amount:
			g.user.wallet.first().decrease(amount)
			user.wallet.first().increment(amount)
			Repo.log_internal("Funded Account of user %s " % str(user.id), g.user.id, amount)
			Repo.log_external("%s Funded your account with %s " % (str(g.user.name), str(amount)), "credit", user.id, g.user.id, amount)
			Repo.log_external("You Funded %s with %s " % (str(user.name), str(amount)), "debit", g.user.id, user.id, amount)
			return jsonify({"status": "success", "message": "credit successful", "data":Repo.get_user_details(user.id)})
		else:
			return jsonify({"status" : "error", "message": "You do not have enough credit to perform this action"}), 404

	else:
		return jsonify({"status" : "error", "message": "User does not exist"}), 404


@api.route('user/details', methods=["GET"])
def get_user_details():
	details = Repo.get_user_details(g.user.id)
	return jsonify({"data":details})


@api.route('recharge', methods=["POST"])
def recharge_wallet():
	#We already implicitly know who is logged in based on their token
	code = request.json.get('code')
	card = models.Voucher.query.filter(models.Voucher.code == code).filter_by(used=False).first()

	if card is not None:
		g.user.wallet.first().increment(card.amount)
		Repo.log_external('Recharged %s' %(str(card.amount)), 'credit', g.user.id,0,card.amount)
		card.used_by = g.user.id
		card.used = True
		db.session.commit()

		return jsonify({"status": "success", "message":"Recharge Successful"}), 200

	else:
		return jsonify({"status": "error", "message": "Card not found or already Used"}), 404


@api.route('password/reset', methods=['POST'])
@exclude_from_token_check
def resetPassword():
	phone = request.json.get('phone')
	user = models.User.query.filter_by(phone = phone).first()
	if user:
		code = user.generate_code()
		Repo.send_sms(phone, "Your password reset code is %s" % (code))
		return jsonify({"status": "success", "message":"Code has been generated"}), 200
	else:
		return jsonify({"status" : "error", "message" : "user not found"}), 404


@api.route('password/reset/confirm', methods=['POST'])
@exclude_from_token_check
def ConfirmResetPassword():
	code 		= request.json.get('code')
	password 	= request.json.get('password')
	phone 		= request.json.get('phone')

	print(phone)

	user = models.User.query.filter_by(phone = phone).first()
	print(user.verify_code(code))
	if user and user.verify_code(code):
		Repo.send_sms(phone, "You Just reset your password")
		user.password = password
		db.session.commit()
		token = user.generate_jwt()
		return jsonify({"status": "success", "message":"Code has been generated", "user": user.as_dict(), "token": token}), 200
	else:
		return jsonify({"status" : "error", "message" : "user not found"}), 404
