import Default from './pages/default.vue';
import Login from './pages/login.vue';
import Register from './pages/register.vue';
import Dashboard from './pages/dashboard.vue';
import Merchant from './pages/merchants';
import Verify from './pages/verify';
import ResetPassword from './pages/resetPassword';

const routes = [
	
	{path:'/home', component: Default, meta:{requiresAuth: false}},
	{path:'/', component: Default, meta:{requiresAuth: false}},
	{path:'/login', component: Login, meta:{requiresAuth: false}},
	{path:'/register', component: Register, meta:{requiresAuth: false}},
	{path:'/dashboard', component: Dashboard, meta:{requiresAuth: true, userOnly: true}},
	{path:'/merchant', component: Merchant, meta:{requiresAuth: true, adminOnly: true}},
	{path:'/verify', component: Verify, meta:{requiresAuth: false}},
	{path:'/password/reset', component: ResetPassword, meta:{requiresAuth: false}}
];

export default routes;