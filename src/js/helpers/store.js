import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
      user: null,
      wallet:null
    },
    mutations: {
      setUser (state, userData) {
        state.user = userData;
      }
    },
    actions: {
      setUser({commit}, user)
      {
        commit('setUser', user)
      }
    }
});

export default store;
