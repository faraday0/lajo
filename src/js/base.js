import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App.vue';
import routes from './routes';
import VueCookie from 'vue-cookie';
import config from '../env';
import swal from 'sweetalert';
import moment from 'moment';
import http from './helpers/http';
import EventBus from './helpers/event-bus';
import VeeValidate from 'vee-validate';
import store from './helpers/store';

// Stylesheets


Vue.mixin({
    data(){
        return{
            http:http,
            config:config,
            eventBus:EventBus
        }
    },
    methods:{
        get_user_item(key,optional=null)
        {
            return store.state.user[key];
        },
        get_cycle_item(key)
        {
            return store.state.user.cycle[key] || null;
        },
        reload_user_data()
        {
            http.get('user/details').then(resp=>{
                VueCookie.set('user_data', JSON.stringify(resp.data));
                store.dispatch('setUser',resp.data);
            })
        }
    }
});

Vue.use(VueRouter);
Vue.use(VueCookie);
Vue.use(VeeValidate);

const router = new VueRouter({
	routes:routes,
    mode: 'history'
});


router.beforeEach((to, from, next) => {
    if(to.path.startsWith('/logout'))
    {
        VueCookie.delete(config.cookie_name);
        VueCookie.delete('user_data');
        window.location = '/';
    }
    if(to.path.startsWith('/login') || to.path.startsWith('/register'))
    {
        if(VueCookie.get(config.cookie_name))
        {
            next({
                path: '/dashboard'
            });
        }
    }
    if(to.meta.requiresAuth)
    {
        if(!VueCookie.get(config.cookie_name))
        {
            next({
                path: '/login',
                query: {
                    redirect: to.path
                }  
            });
        }
        else
        {
            let userData = JSON.parse(VueCookie.get('user_data'));
            store.dispatch('setUser', userData);

            //if somehow, user manages to login, with inactive account, log them out and echo stuff
            if(userData.status != true)
            {
                VueCookie.delete(config.cookie_name);
                next({path:'/'});
            }
            if(to.meta.userOnly && userData.is_agent == true)
            {
                next({path:'/merchant'});
            }
            if(to.meta.adminOnly && userData.is_agent == true)
            {
                if(userData.agent_status == true)
                {
                    next();
                }
                else    
                {

                    EventBus.$emit('agent.not_verified');
                    VueCookie.delete(config.cookie_name);
                    swal('Error', 'Your account has not been Verified, please wait', 'error');
                    next({path:'/'});
                }            
            }
            else if(!to.meta.adminOnly)
            {
                next();
            }
            else{
                next({path:'/dashboard'});
            }
        }
    }
    else
    {
        next();
    }
});

new Vue({
    el: '#app',
    router,
    store,
    template: '<App/>',
    components: { App }
});
