from app import db, models

db.drop_all()
db.create_all()

newAdmin = models.AdminAccounts(email='super@admin.test',password='secret')
db.session.add(newAdmin)
db.session.commit()
