import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    ADMIN                           = os.environ.get('WA_ADMIN')
    ALLOWED_EXTENSIONS              = set(['jpg', 'png'])
    APP_NAME                        = 'Lajo'
    COMMISSION_FEE                  = 0
    DEBUG                           = False
    FLW_API_KEY                     = "tk_sLcWK98ehD1GbMiv5ZQI"
    FLW_MERCHANT_ID                 = "tk_5y1L6AZ0Ed"
    JWT_SECRET_KEY                  = "secret-%-st)ff"
    SECRET_KEY                      = os.environ.get('SECRET_KEY') or 'na-only-you-waka-come'
    SQLALCHEMY_COMMIT_ON_TEARDOWN   = True
    SQLALCHEMY_RECORD_QUERIES       = True
    MAIL_SENDER                     = 'Lajo <admin@lajo.net>'
    MAIL_SUBJECT_PREFIX             = '[Lajo]'
    MONEY_WAVE_API_KEY              = "ts_9BQD3AH5FO1TWPH7KWAA"
    MONEY_WAVE_SECRET               = "ts_CPHYXNWCMVDMXUPQUWMZADJXSQU3DV"
    SLOW_DB_QUERY_TIME              = 0.5
    SMS_AUTH                        = "Basic dGVzdHVvYmlzOnRlc3QxMjM0NQ=="
    SMS_USERNAME                    = "faradayyg@gmail.com"
    SMS_PASSWORD                    = "70c7140204200037dcdd3447c8c92b3a3629d0bb"
    SMS_SENDER                      = "Lajo"
    SSL_DISABLE                     = False
    SQLALCHEMY_TRACK_MODIFICATIONS  = False
    TRANSACTIONS_PER_PAGE           = 5
    UPLOAD_FOLDER                   = os.path.join(basedir, 'user_uploads')
    URL_PREFIX                      = ''
    # SQLALCHEMY_DATABASE_URI         = os.environ.get('DEV_DATABASE_URL') or \

    @staticmethod
    def init_app(app):
        pass


class Development(Config):
    DEBUG                   = True
    SECRET_KEY              = 'development'
    MAIL_SERVER             = 'smtp.gmail.com'
    MAIL_PORT               = 465
    MAIL_USE_TLS            = False
    MAIL_USE_SSL            = True
    MAIL_USERNAME           = "faradayyg@gmail.com"
    MAIL_SUPPRESS_SEND      = False
    MAIL_PASSWORD           = "jamdat52"
            # 'sqlite:///' + os.path.join(basedir, 'data-dev.sqlite')


class Production(Config):
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
                                    'postgres://postgres:jamdat@localhost:5432/lajo' 
        # 'sqlite:///' + os.path.join(basedir, 'data.sqlite')

    @classmethod
    def init_app(cls, app):
        Config.init_app(app)

        # email errors to the administrators
        import logging
        from logging.handlers import SMTPHandler
        credentials = None
        secure = None
        if getattr(cls, 'MAIL_USERNAME', None) is not None:
            credentials = (cls.MAIL_USERNAME, cls.MAIL_PASSWORD)
            if getattr(cls, 'MAIL_USE_TLS', None):
                secure  = ()
        mail_handler    = SMTPHandler(
            mailhost    =(cls.MAIL_SERVER, cls.MAIL_PORT),
            fromaddr    =cls._MAIL_SENDER,
            toaddrs     =[cls.WA_ADMIN],
            subject     =cls.WA_MAIL_SUBJECT_PREFIX + ' Application Error',
            credentials =credentials,
            secure      =secure)
        mail_handler.setLevel(logging.ERROR)
        app.logger.addHandler(mail_handler)


config = {
    'development': Development,
    'production': Production,

    'default': Development
}
